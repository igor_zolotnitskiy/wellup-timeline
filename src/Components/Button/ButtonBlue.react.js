/**
 * Created by Игорь on 12.07.2016.
 */
const styles = require('./Button.less');

const Button = require('./Button.react');

class ButtonBlue extends Button {
    getClassName() {

        return styles.blueButton;
    }
}

module.exports = ButtonBlue;