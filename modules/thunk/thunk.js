/**
 * Created by igor on 04.05.16.
 */
module.exports = function thunkMiddleware({ dispatch, getState }) {
    return next => action => {
        if (typeof action === 'function') {
            return action(dispatch, getState);
        }

        return next(action);
    };
};
